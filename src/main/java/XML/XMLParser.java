package XML;

import EvenementModel.Effet;
import EvenementModel.Evenement;
import EvenementModel.Reponse;
import Faction.Faction;
import Joueur.Joueur;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLOutput;
import java.util.List;
import java.util.Scanner;
import Menu.Menu;


public class XMLParser {

    public  XMLParser() {

    }

    public void start(int event, Faction[] factions, Joueur joueur) {
        try {
            ObjectMapper mapper = new XmlMapper();
            InputStream inputStream = new FileInputStream("src/main/java/Evenement.xml");
            TypeReference<List<Evenement>> typeReference = new TypeReference<List<Evenement>> () {};
            List<Evenement> persons = mapper.readValue(inputStream, typeReference);
            startEvenement(event, factions, joueur, persons);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startEvenement(int evenement, Faction[] factions, Joueur joueur,List<Evenement> persons) {
        System.out.println("----Evenement----");
        System.out.println(persons.get(evenement).getTitre());
        Reponse[] reponse = persons.get(evenement).getReponses();
        int quantiteReponse  = reponse.length;
        String[] reponsesTitle = new String[quantiteReponse];
        stockReponses(reponsesTitle, reponse);
        Menu menuReponse = new Menu("Choisissez l'action à faire", reponsesTitle);
        Scanner scanner = new Scanner(System.in);
        int choix = menuReponse.display(scanner) - 1;
        Effet[] effet = reponse[choix].getEffets();
        applicationEffet(effet, factions, joueur);

    }

    private void applicationEffet(Effet[] effet, Faction[] factions, Joueur joueur) {
        int quantiteEffet = effet.length;
        System.out.println("Le nombre d'effet qui va etre appliqué est de " + quantiteEffet);
        for (int i = 0; i < quantiteEffet; i++) {
            if (effet[i].getEntite() == "Industrie") {
                joueur.modifIndustrie(effet[i].getValue());
            } else if (effet[i].getEntite() == "Agriculture") {
                joueur.modifAgriculture(effet[i].getValue());
            } else {
                int temp = 0;
                while (factions[temp].getNom().equals(effet[i].getEntite()) == false && temp < factions.length - 1 ) {
                    temp++;
                }
                System.out.println(factions[temp].getNom() + " " + effet[i].getEntite() + " " + factions[temp].getNom().equals(effet[i].getEntite()));
                if (temp > factions.length) {
                    System.out.println("Le nom de la faction n'existe pas !! Verifiez votre fichier data!!");
                } else {
                    factions[temp].setSatisfaction(effet[i].getValue());
                }
            }
        }
    }

    private void stockReponses(String[] reponses, Reponse[] reponse) {
        for (int i = 0; i < reponse.length; i++) {
            reponses[i] = reponse[i].getTitre();
        }
    }



}
