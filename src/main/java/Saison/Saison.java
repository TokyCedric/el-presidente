package Saison;

public class Saison {
    int typeSaison;

    public Saison() {
        this.typeSaison = 1;
    }

    public int getTypeSaison() {
        return typeSaison;
    }

    public String getSaison(int numeroSaison) {
        switch (numeroSaison) {
            case 1:
                return "Automne";
            case 2:
                return "Hiver";
            case 3:
                return "Printemps";
            case 4:
                return "Ete";
            default:
                return "lA SAISON N'EXISTE PAS";
        }
    }

    public void setTypeSaison(int typeSaison) {
        this.typeSaison = typeSaison;
    }

    public void prochaineSaison() {

        if (this.typeSaison == 4){
            //lancer bilan fin d'année
            setTypeSaison(0);
        }

        this.typeSaison += 1;
    }
}
