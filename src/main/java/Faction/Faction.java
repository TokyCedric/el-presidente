package Faction;

public class Faction {
    String nom;
    int nombreAdherent;
    int satisfaction;
    boolean active;

    public Faction(String nom, int nombreAdherent, int satisfaction) {
        this.nom = nom;
        this.nombreAdherent = nombreAdherent;
        this.satisfaction = satisfaction;
        this.active = true;
    }

    public String getNom() {
        return nom;
    }

    public int getNombreAdherent() {
        return nombreAdherent;
    }

    public void setNombreAdherent(int nombreAdherent) {
        this.nombreAdherent += nombreAdherent;
    }

    public int getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(int satisfaction) {
        int temp = this.satisfaction + satisfaction;
        if (temp <= 0) {
            setinactif();
        }
        if (isActive()){
            if (temp > 100) {
                this.satisfaction = 100;
            } else {
                this.satisfaction += satisfaction;
            }
        } else {
            this.satisfaction = 0;
        }

    }

    public boolean isActive() {
        return active;
    }

    public void setinactif() {
        this.active = false;
    }


}