package Joueur;

import Faction.Faction;

public class Joueur {
    String nom;
    boolean aPerdu;
    int annee;
    int tresorerie;
    float satisfactionGlobale;
    int industrie;
    int agriculture;
    int avancement;



    public Joueur(String nom) {
        this.nom = nom;
        this.aPerdu = false;
        this.annee = 0;
        this.tresorerie = 200;
        this.satisfactionGlobale = 50 ;
        this.industrie = 40;
        this.agriculture = 59;
    }

    public boolean Perdu() {
        return aPerdu;
    }

    public void setaPerdu() {
        this.aPerdu = true;
    }

    public int getAgriculture() {
        return agriculture;
    }

    public void modifAgriculture (int coef) {
        int agriculture = getAgriculture();
        agriculture += coef;
        int avancement = getAvancement(agriculture, getIndustrie());
        if (avancement <= 100) {
            this.agriculture = agriculture;
        } else {
            int temp = avancement - 100;
            agriculture -= temp;
            this.agriculture = agriculture;
        }
    }

    public int getIndustrie() {
        return industrie;
    }

    public void modifIndustrie (int coef) {
        int industrie = getIndustrie();
        industrie += coef;
        int avancement = getAvancement(industrie,getAgriculture());
        if (avancement <= 100) {
            this.industrie = industrie;
        } else {
            int temp = avancement - 100;
            industrie -= temp;
            this.industrie = industrie;
        }
    }

    public int getAvancement(int agriculture, int industrie) {
        return agriculture + industrie;
    }

    public String etatAvancement() {
        return "\nVotre etat d'avancement est : " + getAvancement(agriculture, industrie) + " (Agriculture : " + getAgriculture() + " Industrie : " + getIndustrie() + ")";
    }

    public void setAvancement(int agriculture, int industrie) {
        avancement = agriculture + industrie;
        this.avancement = avancement;
    }

    public int getTresorerie() {
        return tresorerie;
    }

    public void modifierTresorerie(int somme) {
        this.tresorerie += somme;
    }

    public void modifierSatisfaction(Faction[] faction){
        this.satisfactionGlobale = calculSatisfaction(faction);
    }

    public float calculSatisfaction(Faction[] faction) {
        int temp = 0;
        int population = 0;
        for (int i = 0 ; i < faction.length; i++){
            temp += faction[i].getSatisfaction() * faction[i].getNombreAdherent();
            population += faction[i].getNombreAdherent();
        }

        temp /= population;

        return temp;
    }

    public float getSatisfactionGlobale() {
        return satisfactionGlobale;
    }

    public void setSatisfactionGlobale(Faction[] factions) {
        int crowd = 0;
        int satisfaction = 0;
        for (int i = 0 ; i < factions.length; i++) {
            crowd += factions[i].getNombreAdherent();
            satisfaction += factions[i].getSatisfaction() * factions[i].getNombreAdherent();
        }
        this.satisfactionGlobale = satisfaction / crowd;
    }

    @Override
    public String toString() {
        return "En l'an " + annee + ", le president " + nom + " possede " + tresorerie + " de thune.\n" + "Votre pourcentage globale de satisfaciton est de : " + satisfactionGlobale + etatAvancement();
    }

    public int getAnnée() {
        return annee;
    }

    public void nextAnnee() {
        this.annee += 1;
    }
}
