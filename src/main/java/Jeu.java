import EvenementModel.Evenement;
import Faction.Faction;
import Joueur.Joueur;
import Menu.Menu;
import Saison.Saison;
import XML.XMLParser;

import java.util.Random;
import java.util.Scanner;

public class Jeu {
    public static final int MINIMUM_SATISFACTION = 50;
    private Joueur joueur;
    private Faction[] factions;
    private Saison saison;
    private Evenement evenement;

    public Jeu(Joueur joueur) {
        this.joueur = joueur;
        this.saison = new Saison();
        factions = new Faction[8];
        Faction capitalistes = new Faction("capitalistes", 15, 50);
        Faction communistes = new Faction("communistes", 15, 50);
        Faction libéraux = new Faction("libéraux", 15, 50);
        Faction religieux = new Faction("religieux", 15, 50);
        Faction militaristes = new Faction("militaristes", 15, 50);
        Faction ecologistes = new Faction("ecologistes", 15, 50);
        Faction nationalistes = new Faction("nationalistes", 15, 50);
        Faction loyalistes = new Faction("loyalistes", 15, 100);

        factions[0] = capitalistes;
        factions[1] = communistes;
        factions[2] = libéraux;
        factions[3] = religieux;
        factions[4] = militaristes;
        factions[5] = ecologistes;
        factions[6] = nationalistes;
        factions[7] = loyalistes;

    }

    public void jouer(Joueur joueur, Jeu jeu) {
        boolean finTour = false;
        Random random = new Random();
        Scanner scanner= new Scanner(System.in);

        if(finDannee(jeu.saison.getTypeSaison())) {
            System.out.println("Bonne Année");
            joueur.nextAnnee();
        }

        etatDebutTour();


        // int evenementAleatoire = random.nextInt(1);
        int evenementAleatoire = 0;

        if (evenementAleatoire == 0) {
            //System.out.println("Un evenement correspondant à la saison s'enclenche");
            XMLParser data = new XMLParser();
            int event = random.nextInt(2);
            data.start(event, factions, joueur);
        } else {
            System.out.println("Un evenement aleatoire s'enclenche");
        }

        System.out.println("Etat des factions : ");

        System.out.println("--------Stats-------");
        for (int i = 0; i < factions.length; i++) {
            System.out.println(factions[i].getNom() + " a un taux de satisfaction de : " + factions[i].getSatisfaction());
        }
        joueur.setSatisfactionGlobale(factions);
        System.out.println("La satisfaction globale est de : " + joueur.getSatisfactionGlobale());
        //Test de la satisfaction globale
        testSatifaction(joueur.getSatisfactionGlobale());
        System.out.println("\n\n");

/*
        Menu menuIntermediaire = new Menu("Voulez vous continuez ? ", new String[] {"Oui","Non"});
        int choix = menuIntermediaire.display(scanner);
        System.out.println(choix);
        if (choix == 1) {
            System.out.println("Vous avez choisie de continuez la partie");
        } else {
            joueur.setaPerdu();
            System.out.println("vous avez perdu");
        }
*/

    }

    private void testSatifaction(float satisfactionGlobale) {
        if (satisfactionGlobale < MINIMUM_SATISFACTION) {
            joueur.setaPerdu();
        }
    }

    private void etatDebutTour() {
        System.out.println("Vous etes en " + saison.getSaison(saison.getTypeSaison()) + " de l'année " + joueur.getAnnée());
        System.out.println("Votre trésorerie est de : " + joueur.getTresorerie());
    }

    public boolean finDannee (int saison){
        if (saison == 1) {
            return true;
        } else {
            return false;

        }

    }





}