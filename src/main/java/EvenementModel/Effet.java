package EvenementModel;

public class Effet {
    String entite;
    int value;


    public Effet() {

    }

    public Effet(String entite, int value) {
        super();
        this.entite = entite;
        this.value = value;
    }

    public String getEntite() {
        return entite;
    }

    public void setEntite(String entite) {
        this.entite = entite;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
