package EvenementModel;

import Faction.Faction;

public class Reponse {
    String titre;
    int consequence;
    Effet[] effets;

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Effet[] getEffets() {
        return effets;
    }

    public void setEffets(Effet[] effets) {
        this.effets = effets;
    }

    public int getConsequence() {
        return consequence;
    }

    public void setConsequence(int consequence) {
        this.consequence = consequence;
    }
}
