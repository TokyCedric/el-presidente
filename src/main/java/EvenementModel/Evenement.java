package EvenementModel;

import java.util.ArrayList;

public class Evenement {
    int numero;
    String titre;
    Reponse[] reponses;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Reponse[] getReponses() {
        return reponses;
    }

    public void setReponses(Reponse[] reponses) {
        this.reponses = reponses;
    }
}
