import Joueur.Joueur;
import Menu.Menu;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int choix;
        boolean finPartie = false;
        int nombreTour = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Bienvenue dans le jeu El Presidentex");
        Menu menuPrincipal = new Menu("Choisissez le niveau de difficulté : ",
                new String[]{"Facile", "Moyen", "Difficile"});
        choix = menuPrincipal.display(scanner);
        Joueur player1 = new Joueur("Francis");
        Jeu jeu = new Jeu(player1);
        while (finPartie == false) {
            System.out.println("*----------------------------------*");
            System.out.println("Tour : " + nombreTour);
            jeu.jouer(player1, jeu);
            if (player1.Perdu() == true) {
                System.out.println("--------PARTIE TERMINEE----------");
                System.out.println("Votre taux de satisfaction est descendu au dessous de la barre des 50%");
                finPartie = true;
            } else {
                nombreTour++;
            }
        }

    }
}
